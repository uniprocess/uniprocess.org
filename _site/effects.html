<!-- 
Uniprocess.org, (c) 2018 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
-->
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- Turn off caching in all browsers (BEGIN): 
         Link: http://stackoverflow.com/a/1341133 -->
    <!--
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma"  content="no-cache" />
    -->
    <!-- Turn off caching in all browsers (END): -->
    <meta name="description" content="Uniprocess.org hosted by GitLab" />
    <meta name="author" content="SPISE MISU ApS" />
    <title>Effects - SPISE MISU ApS</title>
    <link rel="canonical" href="//uniprocess.org/" />
    <!-- Local font files placed at GitLab:
    -->
    <link rel="stylesheet" media="screen" href="css/proza-libre.css" type="text/css">
    <!-- Remote font files
    <link
       rel="stylesheet"
       media="screen"
       href="//fontlibrary.org/face/proza-libre"
       type="text/css" />
    -->
    <link rel="stylesheet" type="text/css" href="css/style.css" />
  </head>
  <body>
    <div id="tabs">
  <ul>
    <li>
      <a href="./" class="tab">uniprocess</a>
    </li>
    <li>
      <a href="effects.html" class="tab current">effects</a>
    </li>
    <li>
      <a href="builds.html" class="tab">builds</a>
    </li>
    <li>
      <a href="template.html" class="tab">template</a>
    </li>
    <li>
      <a href="about.html" class="tab">about</a>
    </li>
  </ul>
</div>
<div class="content" style="display: block; width: 640px; margin: 0px auto;">
  <h3 id="purity-vs-effects"><a href="#purity-vs-effects" class="anchor">#</a> Purity vs Effects</h3>
<p>In <code>Haskell</code> there is a clear <code>separation</code>, which is <code>applied</code> through the <code>typing system</code> and the <code>compiler</code>, between <code>pure code</code>: it is always evaluated with the same output value given the same input and does not cause any side effects such as mutation of mutable objects or output to I/O devices; and <code>code</code> that produces <code>effects</code>:</p>
<table>
<thead>
<tr class="header">
<th style="text-align: center;">Parent calls child</th>
<th style="text-align: center;">Parent with effects</th>
<th style="text-align: center;">Parent Pure</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;">Child with effects</td>
<td style="text-align: center;">Code with effects</td>
<td style="text-align: center;"><code>Compiler error</code></td>
</tr>
<tr class="even">
<td style="text-align: center;">Child pure</td>
<td style="text-align: center;">Code with effects</td>
<td style="text-align: center;">Pure code</td>
</tr>
</tbody>
</table>
<p>In some cases, to increase performance, this clear separation can somehow be bypassed with <code>referential transparency</code>. For example:</p>
<pre><code>λ&gt; import System.IO.Unsafe
λ&gt; reftrans = unsafePerformIO $ pure =&lt;&lt; getChar
λ&gt; :t reftrans
λ&gt; reftrans :: Char -- IO effects can't be seen in the signature</code></pre>
<p>When this happens, we can no longer see the <code>side-effects</code> in the function <code>signatures</code> and the type system and compiler, can’t no longer help us.</p>
<blockquote>
<p><strong>Note</strong>: All Haskell applications have a parental code branch with input and output I/O effects. If this were not the case, we could not provide inputs or see the output of the calculation and, therefore, it would be a waste of time to execute any application</p>
</blockquote>
<h3 id="isolation-and-granulation"><a href="#isolation-and-granulation" class="anchor">#</a> Isolation and granulation</h3>
<p>In <code>Haskell</code>, the <code>bridge</code> that is responsible for <code>binding</code> the pure code in combination the with code containing effects, is called <code>monads</code>.</p>
<p><code>Monads</code> are structures that represent calculations defined as a sequence of steps.</p>
<p>As mentioned earlier, this <code>bridge</code> can do so <code>gradually</code> allowing us to make sure that if we allow a part of the code that can access the network, you can only perform that <code>side-effect</code> and not others.</p>
<p>In addition, we also want to provide the possibility to exclude packages that can’t be registered as <code>trusted</code>. This is achieved by introducing the concept of <code>restricted effects</code>, as described in the article <code>[Safe Haskell]</code>, to make sure that only a minimum number of effects can be used. An example combining both granulated as well as restricted effects can be seen in the following lines of code:</p>
<ul>
<li>First we define the monadic effect we want to granulate and we mark the module as <code>Safe</code> code:</li>
</ul>
<pre><code>{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Uniprocess.Effects.Granulated (..) where

--------------------------------------------------------------------------------

class (Monad m) =&gt; CmdM m where
  out :: String -&gt; m ()</code></pre>
<ul>
<li>Next, we define the restricted effects layer as described in <code>[Safe Haskell]</code> and we mark this module as <code>trusted</code>. As we don’t expose the constructor <code>RestrictedIO</code>, we need to implement the restricted instance of the monadic effect that we want to granulate:</li>
</ul>
<pre><code>{-# LANGUAGE Trustworthy #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE UndecidableInstances #-}

--------------------------------------------------------------------------------

module Uniprocess.Effects.Restricted ( .. ) where

--------------------------------------------------------------------------------

newtype RIO a = RestrictedIO { rio :: IO a }

instance Functor RIO where
  fmap f m = RestrictedIO $      f &lt;$&gt; rio m

instance Applicative RIO where
  pure      = RestrictedIO . pure
  (&lt;*&gt;) f m = RestrictedIO $ rio f &lt;*&gt; rio m

instance Monad RIO where
  return    = RestrictedIO . return
  (&gt;&gt;=) m f = RestrictedIO $ rio m &gt;&gt;= rio . f

--------------------------------------------------------------------------------

class Granulated.CmdM m =&gt; CmdM m where
  out :: String -&gt; m ()

instance Granulated.CmdM RIO =&gt; CmdM RIO where
  out x = RestrictedIO $ putStrLn $ x ++ &quot; [Restricted]&quot;</code></pre>
<ul>
<li>Now, we will be able to granulate the restricted effect by simply implementing the granulated monadic effect from the first step:</li>
</ul>
<pre><code>{-# LANGUAGE Safe #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE UndecidableInstances #-}

--------------------------------------------------------------------------------

module Uniprocess.Effects.Granulated.Instances where

--------------------------------------------------------------------------------

instance Granulated.CmdM Restricted.RIO where
  out x =
    Restricted.out $ x ++ &quot; [Granulated]&quot;</code></pre>
<ul>
<li>And finally, we can now limit our application, to only use our subset of effects, by restricting the signature of the only function that the <code>main</code> is bound to:</li>
</ul>
<pre><code>{-# LANGUAGE Trustworthy #-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

isolated :: Eff.RIO ()
main     :: IO ()

--------------------------------------------------------------------------------

isolated = Uni.process

--------------------------------------------------------------------------------

main = Eff.rio . isolated</code></pre>
<ul>
<li>The logic which is called by our application, can therefore be placed in a <code>Safe</code> module of code, which ensure that no undesired side-effects can be performed.</li>
</ul>
<pre><code>{-# LANGUAGE Safe #-}

--------------------------------------------------------------------------------

module Uniprocess.Process ( process ) where

--------------------------------------------------------------------------------

process :: ( Restricted.CmdM m ) =&gt; m ()

--------------------------------------------------------------------------------

process =
  Granulated.out &quot;Uniprocess with isolated (and granulated) side-effects.&quot;</code></pre>
<p>This approach is well known in information security and computer science as <code>principle of least privilege</code> (<code>PoLP</code>) where a <code>process</code>, a user, or a program (depending on the subject) <code>must</code> be able to <code>access only</code> the <code>information</code> and <code>resources</code> that are <code>necessary</code> for its <code>legitimate purpose</code>. <code>Haskell</code>, among very few, can enforce this at <code>compile-time</code>.</p>
<p>Therefore, it is very easy to <code>ensure</code> that the <code>design</code> and <code>architecture</code> will be <code>applied</code> throughout the <code>entire application</code>.</p>
<p>It will also be <code>easy to see</code> for the experts and maybe even for the users, that the <code>application really does</code> what it was <code>designed to do</code>.</p>
<p>And if someone tries to <code>modify</code> the application, with <code>bad intentions</code>, it will <code>require major changes</code> in the design and architecture, which can be <code>easily spotted</code>.</p>
<p>Thanks to the <code>isolation of effects</code>, it would be enough for <code>companies to design</code> the <code>effects layers</code> and <code>outsource the development</code> to anyone, even the best black-hat hackers, with the necessary knowledge, <code>knowing</code> that the <code>code they receive</code> will <code>comply 100%</code> with their <code>initial design</code>.</p>
<p><img src="img/layered-effects.svg" /></p>
<p>Talking about how to do things right and thus ensure <code>data protection</code> by <code>design</code> and by <code>default</code>.</p>
<blockquote>
<p><strong>Note</strong>: And the best thing is that you do not have to believe in my word, you just have to trust a piece of technology that is based on solid foundations of Mathematics and Computers Science</p>
</blockquote>
<p><code>[Safe Haskell]</code>: (David Terei, David Mazières, Simon Marlow, Simon Peyton Jones) Haskell ’12: Proceedings of the Fifth ACM SIGPLAN Symposium on Haskell, Copenhagen, Denmark, ACM, 2012</p>
</div>

    <div class="footer">
      <a href="http://spisemisu.com/" style="text-decoration: none; background-color: rgb(211, 211, 211);">
	<img src="img/sm-logo.svg" title="© 2018 SPISE MISU ApS" style="height: 50px; background-color: rgb(0, 0, 0);" alt="SPISE MISU ApS logo." />
      </a>
      <a href="https://jaspervdj.be/hakyll/" style="text-decoration: none; background-color: rgb(211, 211, 211);">
	<img src="img/hk-logo.svg" title="Proudly generated by Hakyll" style="height: 50px; background-color: rgb(0, 0, 0);" alt="Hakyll logo." />
      </a>
      <a href="https://gitlab.com/" style="text-decoration: none; background-color: rgb(211, 211, 211);">
	<img src="img/gl-logo-extra-whitespace.svg" title="Hosted by GitLab" style="height: 50px; background-color: rgb(0, 0, 0);" alt="GitLab logo." />
      </a>
      <a href="https://www.gnu.org/licenses/agpl.html" style="text-decoration: none; background-color: rgb(211, 211, 211);">
	<img src="img/agpl-v3-logo.svg" title="Website is licensed under AGPL-3.0 (Free as in Freedom, not as in Beer)" style="height: 50px; background-color: rgb(211, 211, 211);" alt="AGPL logo." />
      </a>
      <a href="https://creativecommons.org/licenses/by-sa/4.0/" style="text-decoration: none; background-color: rgb(211, 211, 211);">
	<img src="img/cc-by-sa-icon.svg" title="Media content is licensed under CC BY-SA 4.0" style="height: 50px; background-color: rgb(211, 211, 211);" alt="CC-BY-SA logo." />
      </a>
    </div>
  </body>
</html>
