Uniprocess.org
==============

A `uniprocess` is a stateless piece of software that encapsulates a process,
seen from a business perspective, of which it is known at all times what data
enter and what data comes out of the process. 

For more information, please visit:

* http://uniprocess.org
