{-# LANGUAGE OverloadedStrings #-}

--------------------------------------------------------------------------------

import           Hakyll
import qualified Text.Pandoc      as Pandoc
import qualified Text.Pandoc.Walk as Pandoc.Walk

--------------------------------------------------------------------------------

root :: Routes
root =
  gsubRoute "web/"     (const "") `composeRoutes` idRoute

pages :: Routes
pages =
  gsubRoute "web/pgs/" (const "") `composeRoutes` setExtension "html"

main :: IO ()
main = hakyll $
  do
    match "web/css/*" $ do
      route   root
      compile compressCssCompiler

    match "web/css/proza-libre/*" $ do
      route   root
      compile copyFileCompiler

    match "web/img/*" $ do
      route   root
      compile copyFileCompiler

    match "web/pgs/about.md" $ do
        route   $ pages
        compile $ anchorsPandocCompiler
          >>= loadAndApplyTemplate "web/tpl/about.html"   defaultContext
          >>= loadAndApplyTemplate "web/tpl/default.html" defaultContext
          >>= relativizeUrls

    match "web/pgs/builds.md" $ do
        route   $ pages
        compile $ anchorsPandocCompiler
          >>= loadAndApplyTemplate "web/tpl/builds.html"  defaultContext
          >>= loadAndApplyTemplate "web/tpl/default.html" defaultContext
          >>= relativizeUrls

    match "web/pgs/effects.md" $ do
        route   $ pages
        compile $ anchorsPandocCompiler
          >>= loadAndApplyTemplate "web/tpl/effects.html" defaultContext
          >>= loadAndApplyTemplate "web/tpl/default.html" defaultContext
          >>= relativizeUrls

    match "web/pgs/template.md" $ do
        route   $ pages
        compile $ anchorsPandocCompiler
          >>= loadAndApplyTemplate "web/tpl/template.html" defaultContext
          >>= loadAndApplyTemplate "web/tpl/default.html"  defaultContext
          >>= relativizeUrls

    match "web/pgs/index.md" $ do
        route   $ pages
        compile $ anchorsPandocCompiler
          >>= loadAndApplyTemplate "web/tpl/home.html"    defaultContext
          >>= loadAndApplyTemplate "web/tpl/default.html" defaultContext
          >>= relativizeUrls

    match "web/tpl/*" $ do
      compile templateCompiler

--------------------------------------------------------------------------------

-- HELPERS

-- https://github.com/haskell-org/summer-of-haskell/blob/master/src/Main.hs#L72

-- | Our own pandoc compiler which adds anchors automatically.
anchorsPandocCompiler :: Compiler (Item String)
anchorsPandocCompiler = pandocCompilerWithTransform
    defaultHakyllReaderOptions
    defaultHakyllWriterOptions
    addAnchors

-- | Modifie a headers to add an extra anchor which links to the header.  This
-- allows you to easily copy an anchor link to a header.
addAnchors :: Pandoc.Pandoc -> Pandoc.Pandoc
addAnchors =
  Pandoc.Walk.walk addAnchor
  where
    addAnchor :: Pandoc.Block -> Pandoc.Block
    addAnchor (Pandoc.Header level attr@(id_, _, _) content) =
      Pandoc.Header level attr $
      [ Pandoc.Link
          ("", ["anchor"], [])
          [Pandoc.Str "#"]
          ("#" <> id_, "")
      ] ++ [Pandoc.Str " "] ++ content
    addAnchor block = block

--------------------------------------------------------------------------------
