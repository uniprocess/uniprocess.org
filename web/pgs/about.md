---
title: About
---

### Advice, guidance and help

Here is a list of some companies that would be able to help you create
`uniprocesses` for your `business`:

* [SPISE MISU ApS][spisemisu]: It's the company who `researched` this approach
  on how to make software that could live up to the `EU GDPR`, please read the
  **Legislation** section below. The presented approach will allow companies who
  uses this concept to utilize their resources in the making of software,
  instead of misusing them on other less helpful ways as for example
  `bureaucratic paperwork`.

  Soon to come, the commercial site, where `business analysts`, with no software
  developing skills, will be able to define `uniprocesses`. Afterwards they will
  be able to download an updated `template` with the `custom` process
  definitions from which the `implementation` can be outsourced to any software
  provider, with the right skill set of course. Once they receive the
  `implementation`, they can re-upload the `template` + `implementation`. If the
  solution `builds`, they will receive the `bits` of the `uniprocess` as well as
  a `distribution container`, signed by [SPISE MISU ApS][spisemisu], with their
  respective `reproducible build hashes`. These `hashes` will provide them with
  a `certification mark` that the `implementation` actually lives up to the
  `process` definition and complies with the concept of `uniprocess` definitions
  described on this website:

    * [Uniprocess(es) (Coming soon)][uniprocesses]

  Services like training, consulting and development are also provided, but to a
  lesser extent.

* Europe region

    * [Tweag.io][tweagio]: Located in (France - HQ, Cyprus and United Kingdom)
      and is the leading company in Europe providing the following `Haskell`
      services:
	  
        * Training
		* Consulting
		* Projects and development

    * [Well-Typed][welltyped]: Located in United Kingdom and providing the
      following `Haskell` services, among others to [IOHK][iohk]:
	  
        * Development
        * Maintenance
        * Advice
        * Training

* North America

    * [FPComplete][fpcomplete]: Located in the United States (San Diego) and
      providing the following `Haskell` and `Docker` services:

        * Training
        * Consulting
        * Projects
        * Development

    * [Galois][galois]: Located in the United States (Portland, Arlington and
      Dayton) and providing the following `Haskell` services:

        * Research
        * Consulting
        * Development

Please don't hesitate to provides us with a `pull request` in order for us to
update this section with your information.

> **Note**: We reserve the right not to update the section if we can't confirm
> the validity of the provided information.

[iohk]:         https://iohk.io/
[uniprocesses]: http://uniprocesses.com/
[spisemisu]:    http://spisemisu.com/
[tweagio]:      https://www.tweag.io/how-we-work
[welltyped]:    https://www.well-typed.com/services/
[fpcomplete]:   https://www.fpcomplete.com/pricing
[galois]:       https://galois.com/services/

### Legislation

Here is a list of the [Directive 95/46/EC][eugdpr] (`EU GDPR`) articles that the
`uniprocesses` cover and with a short explanation on how:

* **§05 - Principles relating to processing of personal data**:

    1. Personal data shall be `processed` with:

        * “`lawfulness`, `fairness` and `transparency`” (§ 5.1.a)

        * “`purpose limitation`” (§ 5.1.b)

        * “`data minimization`” (§ 5.1.c)

        * “`accuracy`” (§ 5.1.d)

        * “`storage limitation`” (§ 5.1.e)

        * “`integrity` and `confidentiality`” (§ 5.1.f)

    2. The `controller` shall be responsible for, and be able to demonstrate
       `compliance` with § 5.1 (`accountability`)

* **§ 25 - Data protection by design and by default**:

	1. Taking into account the `state of the art`, the cost of implementation
       and the nature, scope, context and purposes of processing as well as the
       risks of varying likelihood and severity for rights and freedoms of
       natural persons posed by the processing, the controller shall, both at
       the time of the determination of the means for processing and at the time
       of the processing itself, implement `appropriate technical` and
       organizational measures, such as pseudonymisation, which are `designed`
       to implement `data-protection` principles, such as data minimization, in
       an effective manner and to integrate the necessary `safeguards` into the
       processing in order to meet the requirements of this Regulation and
       protect the rights of data subjects.

	2. The controller shall implement `appropriate technical` and organizational
       measures for ensuring that, `by default`, only personal data which are
       necessary for each specific purpose of the processing are processed. That
       obligation applies to the amount of personal data collected, the extent
       of their processing, the period of their storage and their
       accessibility. In particular, such measures shall ensure that by default
       personal data are not made accessible without the individual's
       intervention to an indefinite number of natural persons.

	3. An approved certification mechanism pursuant to Article 42 may be used as
       an element to demonstrate compliance with the requirements set out in
       paragraphs 1 and 2 of this Article.

It's obvious on how these articles are covered with the presented concept of
`uniprocess` as we will have absolute control over which and how data flows
through a process by using `state of the art` technology, built on sound
foundations of Mathematics and Computer Science. Our concept might differ from
other that we are able to solve `Article § 25.3` as we built in the mechanism to
demonstrate compliance directly into the software applications which will allow
for easier and more trustworthy audits when performed by the authorities.

[eugdpr]: https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32016R0679
