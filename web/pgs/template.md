---
title: Template
---

### Release Notes

-------------------------------------------------------------------------------
  Version    Comments                                                  Date
------------ ----------------------------------------------------- ------------
  0.11.0.0   Initial λ Release                                       2018 OCT
-------------------------------------------------------------------------------

### Haskell Template

In order for the `implementation` to `comply` with what is `described` on this
`website`, it is necessary that the `code` can be `marked` as a `secure` code
with the following compiler flags:

```
... -XSafe -fpackage-trust -trust=base ...
```

This have the consequence that some `Haskell` packages can't be used, as for
example `Data.Text` since it can't be marked as `trustworthy`, while other like
`Data.ByteString` can be marked and therefore be used.

For more information on the `template`, please look into the source code which
can be found at:

* [Uniprocess Template @ GitLab][gitlab]

[gitlab]: https://gitlab.com/uniprocess/haskell-template/

### Open Source

We have chosen [LGPL-3.0][lgpl3] as it is a permissive [copyleft][copyleft]
license, that will allow you to build on the provided solution but letting you
decide if your work is going to get released under another license, open source
or not.

![](img/lgpl-v3-logo-wide.svg)

With regard of the media content and since this is not our strong side, we have
decided to release under another [copyleft][copyleft] license, [CC BY-SA
4.0][ccbysa4], as it will allow people contributing with media content, to get
the credits they deserve, while others still can use their work in a commercial
manner.

![](img/cc-by-sa-icon-wide.svg)

[copyleft]: https://copyleft.org/
[ccbysa4]:  https://creativecommons.org/licenses/by-sa/4.0/
[lgpl3]:    https://www.gnu.org/licenses/lgpl-3.0.html  


### Contribute

There are only three ways you can contribute:

* Donate well written code or create media content

* Donate design/code reviews with constructive criticism

* Donate financial means

> **Note**: Donations to Danish companies can’t be anonymous due to legislation
> regarding money laundering. Please ensure that your donation is linked to your
> information; due to resource limitations, we cannot provide any refunds.
