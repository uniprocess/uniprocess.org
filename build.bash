#!/usr/bin/env bash

################################################################################
##
## Uniprocess.org, (c) 2018 SPISE MISU ApS, opensource.org/licenses/AGPL-3.0
##
################################################################################

clear

bld="$(pwd)/.stack-work/install/"
tgt="$(pwd)/bin"

echo "### Ensure folders exist:"
mkdir -v -p $bld;
mkdir -v -p $tgt;
echo

echo "### Clearing binaries:"
find $bld -mindepth 1 -name "*" -delete -print
find $tgt -mindepth 1 -name "*" -delete -print
echo

echo "### Stack building:" 
#stack clean
#stack build --verbosity debug
#stack build --fast
stack build
echo

src="$(stack path --local-install-root)/bin"

echo "### Ensure folder exist:"
mkdir -v -p $src;
echo

echo "### Copying binaries to local $tgt:" 
cp -Rv $src/. $tgt/
echo

echo "### Clearing all .cabal files:" 
find . -name "*.cabal" -delete -print
echo

echo "### Repoducible hashes:"
cd $tgt
for f in *; do
    # skip all non-binaries
    [[ $f == *.* ]] && continue
    echo -e $(sha256sum $f | cut -d " " -f 1): $f
done;
echo

echo "### Cleaning, building and serving the static site:"
for f in *; do
    # skip all non-binaries
    [[ $f == *.* ]] && continue
    cd ..
    echo "$tgt/./$f clean"
    $tgt/./$f clean
    echo "$tgt/./$f build"
    $tgt/./$f build
    echo "$tgt/./$f server"
    $tgt/./$f server
done;
echo
